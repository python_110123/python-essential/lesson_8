# Створіть список товарів в інтернет-магазині. Серіалізуйте
# його за допомогою pickle та збережіть у JSON.
import pickle
import json

# Список товарів в інтернет-магазині
products = [
    {"name": "Laptop", "price": 1000},
    {"name": "Phone", "price": 500},
    {"name": "Headphones", "price": 100},
]

# Збереження у форматі pickle
with open("products.pickle", "wb") as file:
    pickle.dump(products, file)

# Загрузка серіалізованого списку продуктів з pickle файлу
with open("products.pickle", "rb") as file:
    loaded_products = pickle.load(file)

# Перетворення завантаження списку продуктів на JSON формат
json_products = json.dumps(loaded_products, indent=4)

# Збереження у форматі JSON
with open("products.json", "w", encoding="UTF-8") as file:
    file.write(json_products)
