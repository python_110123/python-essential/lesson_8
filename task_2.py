# Модифікуйте вихідний код сервісу зі скорочення посилань з ДЗ 7
# заняття курсу Python Starter так, щоб він зберігав базу посилань
# на диску і не «забув» при перезапуску. За бажанням можете ознайомитис
# з модулем shelve (https://docs.python.org/3/library/shelve.html),
# який у даному випадку буде дуже зручним та спростить виконання
# завдання.
import json


def load_links():
    """Функція для завантаження бази посилань з файлу"""
    try:
        with open("links.json", "r", encoding="UTF-8") as file:
            return json.load(file)
    except FileNotFoundError:
        return {}


def save_links(links):
    """Функція для збереження бази посилань у файл"""
    with open("links.json", "w", encoding="UTF-8") as file:
        json.dump(links, file)


links = load_links()

original_link = input("Введіть початкове посилання: ")
short_name = input("Введіть коротку назву: ")
if short_name not in links:
    links[short_name] = original_link
    print(f"Коротке посилання: {short_name}")
else:
    print("Назва зайнята, введіть іншу")
# отримання початкового посилання за її назвою
requested_short_name = input("Введіть коротку назву, щоб отримати повне посилання: ")
if links.get(requested_short_name):
    print(
        f"Початкове посилання для {requested_short_name}: {links[requested_short_name]}"
    )
else:
    print("Посилання з такою короткою назвою не знайдено.")

# зберігання зміненої бази посилань у файл
save_links(links)
